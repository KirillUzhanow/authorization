﻿<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title> Авторизация	</title>
	<style>	
		.container{
			width:450px;
            height: 450px;
			margin: 150px auto 0 auto;
			background-color:#69a4a2;
			text-align: center;
			padding-top:50px;
			padding-bottom: 30px;
			display: block;
		}
		input[type="email"],input[type="password"]{
			width: 300px;
			height: 50px;
			font-size: 18px;
			padding-left: 30px;
			margin-bottom: 25px;
		}
		input[type='submit']{
			height: 50px;
			width: 90px;
			font-size: 15px;
			background-color: #2ca8c6;
			color: white;
			border: none;
			cursor: pointer;
		}

		.container a{
			color: white;
		}
		p{
			font-size: 30px;
			color: white;
			font-family: Helvetica;

		}
		.error{
			color: red;
			font-size: 25px;
		}
		.v{
			color: green;
			font-size: 25px;
		}

		
	</style>
</head>
<body>
	<div class="container">

		<div class="error">
			<?php 
			if(!empty($error))
				echo $error;

			 ?>

		</div>
		<div class="v">
			<?php 
			if(!empty($v))
				echo $v;
			 ?>
		</div>
		 <p>Авторизация</p>
		<form action="index.php" method="post">
			<p> <input name="emaili" type="email" placeholder="Введите e-mail"  ></p>
			<p> <input name="passwordi" type="password" placeholder="Введите пароль"  ></p>
			<p><input type="submit" name="do_login"></p>
			<a href="#">Регистрация</a>

		</form>
	</div>
</body>
</html>